<?php

namespace Drupal\function_autoload;

use \Drupal\Core\Extension\ExtensionList;
use \Drupal\Core\Extension\ModuleHandler;
use \Drupal\Core\Extension\ThemeHandler;
use \Drupal\Core\File\FileSystem;
use \Symfony\Component\Yaml\Yaml;

class FunctionAutoloader {
  /**
  * The module extension list service.
  */
  protected $moduleHandler;

  /**
   * The module extension list service.
   */
  protected $themeHandler;

  /**
   * The file system service.
   */
  protected $fileSystem;

  /**
   * Constructs a new FunctionAutoloader object
   */
  public function __construct(
    ModuleHandler $moduleHandler,
    ThemeHandler $themeHandler,
    FileSystem $fileSystem,
  ) {
    $this->moduleHandler = $moduleHandler;
    $this->themeHandler = $themeHandler;
    $this->fileSystem = $fileSystem;
  }

  public function load() {
    $extensions = array_filter(
      array_merge(
        $this->moduleHandler->getModuleList(),
        $this->themeHandler->listInfo(),
      ),
      function ($extension) {
        $extension_info = Yaml::parse(file_get_contents($extension->getPathname()));
        return $extension_info['function_autoload']['enabled'] ?? false;
      }
    );

    foreach ($extensions as $extension) {
      $dir = $extension->getPath() . '/' . 'functions'; // TODO: parameterize
      if (!is_dir($dir)) continue;
      $mask = $extension->getType() === 'module' ? '/\.module$/' : '/\.theme$/';
      foreach ($this->fileSystem->scanDirectory($dir, $mask) as $file) {
        include $file->uri;
      }
    }
  }
}
